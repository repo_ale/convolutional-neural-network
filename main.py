import numpy as np
from scipy import signal

def tanh(x):
    return np.tanh(x)
def d_tanh(x):
    return 1 - np.tanh(x) ** 2

def log(x):
    return 1/(1 + np.exp(-1*x))
def d_log(x):
    return log(x) * ( 1 - log(x) )

#np.random.seed(12212)

x1 = np.array([[0,0,0], [0,0,0], [0,0,0]])
x2 = np.array([[1,1,1], [0,0,0], [0,0,0]])
x3 = np.array([[0,0,0], [1,1,1], [1,1,1]])
x4 = np.array([[1,1,1], [1,1,1], [1,1,1]])

X = [x1, x2, x3, x4]

Y = np.array([
    [0.53],
    [0.77],
    [0.88],
    [1.1]
])

# 0. Declare Weights
w1 = np.random.randn(2,2) * 4
w2 = np.random.randn(4,1) * 4

# 1. Declare hyper Parameters
num_epoch = 10000
learning_rate = 0.7

cost_before_train = 0
cost_after_train = 0
final_out, start_out = np.array([[]]), np.array([[]])

# cost before training
for i in range(len(X)):
    layer_1 = signal.convolve2d(X[i], w1, 'valid')
    layer_1_act = tanh(layer_1)

    layer_1_act_vec = np.expand_dims(np.reshape(layer_1_act, -1), axis=0)

    layer_2 = layer_1_act_vec.dot(w2)
    layer_2_act = log(layer_2)

    cost = np.square(layer_2_act - Y[i]).sum() * 0.5
    cost_before_train = cost_before_train + cost
    start_out = np.append(start_out, layer_2_act)

# training
for iter in range(num_epoch):
    
    for i in range(len(X)):
        layer_1 = signal.convolve2d(X[i], w1, 'valid')
        layer_1_act = tanh(layer_1)

        layer_1_act_vec = np.expand_dims(np.reshape(layer_1_act, -1), axis=0)
        
        layer_2 = layer_1_act_vec.dot(w2)
        layer_2_act = log(layer_2)

        cost = np.square(layer_2_act - Y[i]).sum() * 0.5

        # print("Current iter : ", iter, " Current train: ", i, " Current cost: ", cost, end="\r")

        grad_2_part_1 = layer_2_act - Y[i]
        grad_2_part_2 = d_log(layer_2)
        grad_2_part_3 = layer_1_act_vec

        grad_2 = grad_2_part_3.T.dot(grad_2_part_1 * grad_2_part_2)

        d_net_j = w2.dot(grad_2_part_1 * grad_2_part_2) * d_log(layer_2_act)

        #print d_net_j
        
        kernel_reshaped = w1.reshape(1,-1)[0]
        kernel_gradient = np.zeros(w1.shape).reshape(1,-1)[0]
        #kernel_gradient = np.arange(4).reshape(2,2).reshape(1,-1)[0]
        
        kernel_gradient = kernel_gradient.reshape(2,2)
        
        kernel_gradient = kernel_gradient.reshape(1,-1)[0]
        
        d_net_j_reshaped = d_net_j.reshape(1, -1)[0]

        for i_k in range(len(kernel_reshaped)):
            k = kernel_reshaped[i_k]
            k_g = kernel_gradient[i_k]
            
            
            reshaped_input = X[i].reshape(1, -1)[0]
            
            sum_d_kernel = 0

            input_ak_coef = 0

            if(i_k >= 3):
                input_ak_coef = 1

            for d_net_j_index in range(len(d_net_j_reshaped)):
                actual_d_net = d_net_j_reshaped[d_net_j_index]
                input_a_coef = input_ak_coef + i_k
                jump_coef = 0
                if(d_net_j_index >= 3):
                    jump_coef = 1
                    
                sum_d_kernel += actual_d_net * reshaped_input[d_net_j_index+input_a_coef+jump_coef]
                kernel_gradient[i_k] = sum_d_kernel
            
        #print kernel_gradient.reshape(2,2)
        #print w1
        w1 = w1 - kernel_gradient.reshape(2,2) * learning_rate

        #print grad_2
        #print w2
        w2 = w2 - grad_2 * learning_rate
        
                

# cost after training
for i in range(len(X)):
    layer_1 = signal.convolve2d(X[i], w1, 'valid')
    layer_1_act = tanh(layer_1)

    layer_1_act_vec = np.expand_dims(np.reshape(layer_1_act, -1), axis=0)
    
    layer_2 = layer_1_act_vec.dot(w2)
    layer_2_act = log(layer_2)

    cost = np.square(layer_2_act - Y[i]).sum() * 0.5

    cost_after_train = cost_after_train + cost

    final_out = np.append(final_out, layer_2_act)

# print results
print("\nW1 :",w1, "\n\nw2 :", w2)
print("----------------")
print("Cost before Training: ",cost_before_train)
print("Cost after Training: ",cost_after_train)
print("----------------")
print("Start Out put : ", start_out)
print("Final Out put : ", final_out)
print("Ground Truth  : ", Y.T)
